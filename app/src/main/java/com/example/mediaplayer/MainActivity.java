package com.example.mediaplayer;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity {

    Context A = this;

    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPageAdapter adapter = new ViewPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new SoundFragment(),"Sound");
        adapter.addFragment(new VideoFragment(),"Video");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

                                                       }//onCreate

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this,SoundPlayerService.class));
                               }//onDestroy
                                                        }//MainActivity
