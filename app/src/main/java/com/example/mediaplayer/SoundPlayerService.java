package com.example.mediaplayer;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;

import static android.content.Intent.FLAG_RECEIVER_FOREGROUND;
import static java.lang.Thread.sleep;

public class SoundPlayerService extends IntentService {

    private MediaPlayer mediaPlayer = null;
    public String songName;
    public String timeToSend="0";
    int currentTime = 0;
    public long timeToString=0;
    private BroadcastReceiver broadcastReceiver;
    private BroadcastReceiver broadcastReceiver2;
    public static boolean continueRunning = true;


    public SoundPlayerService(){
        super("com.example.mediaplayer.SoundPlayerService");
                               }//SoundPlayerService

    public SoundPlayerService(String name) {
        super(name);
                                           }//SoundPlayerService

    @Override
    public void onCreate() {
        RunningSongName.Name = null;
        currentTime=0;
        timeToSend="0";
        timeToString=0;
        continueRunning = true;
        super.onCreate();

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        songName = intent.getStringExtra("Name");
        RunningSongName.Name = songName;
        mediaPlayer = MediaPlayer.create(this, Uri.parse(songName));

        mediaPlayer.start();
        Intent intent1 = new Intent("MEDIA_PLAYER_SEEK_BAR_ACTION");
        intent1.setFlags(FLAG_RECEIVER_FOREGROUND);

        broadcastReceiver2 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, final Intent intent) {
                mediaPlayer.seekTo(intent.getIntExtra("position", 0));
                                                                        }//onReceive
                                                     };//BroadcastReceiver
        registerReceiver(broadcastReceiver2,new IntentFilter("Seek_To_Touch"));


        int totalTime = mediaPlayer.getDuration();

        while (currentTime < totalTime) {

            if(continueRunning == false){
                continueRunning = true;
                stopSelf();
            }

            try {

                    sleep(500);

                    timeToString = mediaPlayer.getCurrentPosition();

                    int seconds = (int) (timeToString / 1000) % 60 ;
                    int minutes = (int) ((timeToString / (1000*60)) % 60);
                    int hours   = (int) ((timeToString / (1000*60*60)) % 24);

                    String Seconds,Minutes,Hours;

                    if (seconds < 10)
                        Seconds = "0" + seconds;
                    else
                        Seconds = "" + seconds;

                    if (minutes < 10)
                        Minutes = "0" + minutes;
                    else
                        Minutes = "" + minutes;

                    if (hours < 10)
                        Hours = "0" + hours;
                    else
                        Hours = "" + hours;

                     timeToSend = Hours + ":" + Minutes + ":" + Seconds;


                intent1.putExtra("position", timeToString).putExtra("current Time",timeToSend);
                sendBroadcast(intent1);
                currentTime = mediaPlayer.getCurrentPosition();


                 }//try

            catch (InterruptedException e) {}//catch


                                          }//while loop

                                                  }//onHandleIntent

    @Override
    public void onDestroy() {

        if (mediaPlayer != null) {
            mediaPlayer.stop();
            RunningSongName.Name = null;
            currentTime=0;
            timeToSend="0";
            timeToString=0;
                                 }//if statement
        super.onDestroy();

                            }//onDestroy



                                                   }//SoundPlayerService
