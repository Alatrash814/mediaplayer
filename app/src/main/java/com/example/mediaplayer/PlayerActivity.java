package com.example.mediaplayer;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import static android.content.Intent.FLAG_RECEIVER_FOREGROUND;


public class PlayerActivity extends AppCompatActivity {

    private Thread seekBarPosition = null;
    private int position = 0;
    private ArrayList<File> mySongs;
    private Intent myIntent = null;
    private Bundle bundle = null;
    private String songName = null;
    private TextView SongNameLabel = null;
    private TextView songLength = null;
    private int lastPosition;
    private String tempSongName = null;

    private SeekBar seekBar = null;
    private long totalDuration = 0;
    private int currentPosition = 0;
    private BroadcastReceiver broadcastReceiver = null;
    private Context context;
    private TextView currentTime;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.playerlayout);
        context = this;
        myIntent = getIntent();
        bundle = myIntent.getExtras();
        songName = myIntent.getStringExtra("Song Name");
        SongNameLabel = findViewById(R.id.SongNameLabel);
        lastPosition = songName.lastIndexOf('/');
        tempSongName = songName.substring(lastPosition + 1,songName.length());
        SongNameLabel.setText(tempSongName);
        position = bundle.getInt("Position");
        songLength = findViewById(R.id.SongLength2);
        songLength.setText(bundle.getString("song Length").substring(3,bundle.getString("song Length").length()));
        songLength = findViewById(R.id.SongLength);
        songLength.setText(bundle.getString("song Length").substring(3,bundle.getString("song Length").length()));
        songLength = findViewById(R.id.CurrentTime);
        songLength.setText("00:00");
        totalDuration = bundle.getLong("Length");
        seekBar = (SeekBar) findViewById(R.id.SeekBar);
        currentTime = (TextView) findViewById(R.id.CurrentTime);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Intent intent = new Intent("Seek_To");
                intent.setFlags(FLAG_RECEIVER_FOREGROUND);
                long SeekBarPro = seekBar.getProgress();
                intent.putExtra("position", SeekBarPro);
                sendBroadcast(intent);
                                                              }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Intent intent = new Intent("Seek_To_Touch");
                intent.setFlags(FLAG_RECEIVER_FOREGROUND);
                int SeekBarPro = seekBar.getProgress();
                intent.putExtra("position", SeekBarPro);
                sendBroadcast(intent);
                                                             }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, final Intent intent) {
                RunningSongName.currentTime = intent.getLongExtra("position",0);
                seekBar.setProgress((int)RunningSongName.currentTime);
                currentTime.setText(intent.getStringExtra("current Time"));
                                                                        }//onReceive
                                                     };//BroadcastReceiver

        registerReceiver(broadcastReceiver,new IntentFilter("MEDIA_PLAYER_SEEK_BAR_ACTION"));

        if (!checkServiceRunning()) {

            seekBar.setMax((int)(totalDuration));
            seekBar.setProgress(0);
            Intent intent = new Intent(this, SoundPlayerService.class);
            intent.putExtra("Name", songName);

            startService(intent);

                                     }//if statement


        else if (checkServiceRunning() && !RunningSongName.Name.equals(songName)) {

            seekBar.setMax((int)(totalDuration));
            seekBar.setProgress(0);
//            stopService(RunningSongName.intent);
            SoundPlayerService.continueRunning = false;


            Intent intent = new Intent(this, SoundPlayerService.class);
            intent.putExtra("Name", songName);

            startService(intent);

                                                                                   }//else if statement

        else if (checkServiceRunning() && RunningSongName.Name.equals(songName)) {
            seekBar.setMax((int) (totalDuration));
            seekBar.setProgress((int) RunningSongName.currentTime);
                                                                                 }//else if statement

                                                        }//onCreate

    public boolean checkServiceRunning(){
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
            if ("com.example.mediaplayer.SoundPlayerService"
                    .equals(service.service.getClassName()))
                return true;
        return false;
                                         }//checkServiceRunning

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();

                               }//onDestroy



                                                         }//PlayerActivity
