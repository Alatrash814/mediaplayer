package com.example.mediaplayer;

import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.util.ArrayList;


public class SoundFragment extends Fragment {

    MediaMetadataRetriever mediaMetadataRetriever = null;
    private ArrayList<File> songsFiles = null;
    private ArrayList<SongInfo> Songs = new ArrayList<>();
    private SongAdapter Adapter = null;
    private String songName = null;
    private ImageView icon = null;
    private TextView songDec = null;
    private TextView songLength = null;
    private SongInfo songInfo = null;
    public Intent intent = null;
    View view;
    public SoundFragment (){}//SoundFragment

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.soundfragment, container, false);
        runPermission();
        return view;
                                                                                                      }//onCreateView

    public ArrayList<File> FindSongs (File file){

        ArrayList<File>songs = new ArrayList<>();
        File [] files = file.listFiles();

        for (File singleFile : files){
            if (singleFile.isDirectory() && !singleFile.isHidden())
                songs.addAll(FindSongs(singleFile));

            else
            if (singleFile.getName().endsWith(".mp3") || singleFile.getName().endsWith(".wav"))
                songs.add(singleFile);

                                      }//for loop

        return songs;
                                                }//FindSongs

    public void Display () {

        final ListView temp = view.findViewById(R.id.songListView);


        songsFiles = FindSongs(Environment.getExternalStorageDirectory());



            for (int i = 0; i < songsFiles.size(); i++) {

                mediaMetadataRetriever = new MediaMetadataRetriever();
                mediaMetadataRetriever.setDataSource(songsFiles.get(i).getAbsolutePath());
                songInfo = new SongInfo(songsFiles.get(i).getName(),
                        formateMilliSeccond(Long.parseLong(mediaMetadataRetriever.extractMetadata
                                (MediaMetadataRetriever.METADATA_KEY_DURATION))),
                        songsFiles.get(i), i, Long.parseLong(mediaMetadataRetriever.extractMetadata
                        (MediaMetadataRetriever.METADATA_KEY_DURATION)));
                Songs.add(songInfo);

                                                         }//for loop


            Adapter = new SongAdapter(getActivity(), songsFiles);
            temp.setAdapter(Adapter);

            temp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    songName = temp.getItemAtPosition(position).toString();

                    startActivity(new Intent(getActivity(), PlayerActivity.class).putExtra("Songs", songsFiles)
                            .putExtra("Song Name", Songs.get(position).SongFile.getAbsoluteFile().toString()).putExtra("Position", position)
                            .putExtra("song Length", Songs.get(position).SongLength)
                            .putExtra("Length", Songs.get(position).Length));

                                                                                                  }//onItemClick
                                                                               });

                            }//Display

    class SongAdapter extends BaseAdapter {

        ArrayList<File> ListOfSongs;
        Context context = null;


        public SongAdapter(Context context, ArrayList<File> ListOfSongs) {
            super();
            this.ListOfSongs = ListOfSongs;
            this.context = context;
                                                                         }//constructor

        @Override
        public int getCount() {
            return ListOfSongs.size();
                              }//getCount

        @Override
        public Object getItem(int position) {
            return ListOfSongs.get(position);
                                            }//getItem

        @Override
        public long getItemId(int position) {
            return new Long("" + position);
                                            }//getItemId

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            songInfo = Songs.get(position);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View MyView = inflater.inflate(R.layout.songslayout, null);
            icon = MyView.findViewById(R.id.MusicIcon);
            icon.setImageResource(R.drawable.music);
            songDec = MyView.findViewById(R.id.SongDes);
            songDec.setText(songInfo.SongName);
            songLength = MyView.findViewById(R.id.SongLength);
            songLength.setText(Songs.get(position).SongLength);


            return MyView;
                                                                               }//getView

                                            }//SongAdapter

    public void runPermission (){

        Dexter.withActivity(getActivity())
                .withPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {

                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        Display();
                                                                                        }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) { }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                                                                                                                        }
                }).check();

                                }//runPermission

    public String formateMilliSeccond(Long milliseconds){

        int seconds = (int) (milliseconds / 1000) % 60 ;
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24);

        String Seconds,Minutes,Hours;

        if (seconds < 10)
            Seconds = "0" + seconds;
        else
            Seconds = "" + seconds;

        if (minutes < 10)
            Minutes = "0" + minutes;
        else
            Minutes = "" + minutes;

        if (hours < 10)
            Hours = "0" + hours;
        else
            Hours = "" + hours;

        return Hours + ":" + Minutes + ":" + Seconds;

                                                        }//formateMilliSeccond

    class SongInfo {

        public String SongName;
        public String SongLength;
        public int SongPosition;
        public File SongFile;
        public long Length;

        public SongInfo(){
            SongName = SongLength = null;
            SongFile = null;
            SongPosition = 0;
            Length = 0;
                         }//Default Constructor

        public SongInfo (String Name, String SLength , File file , int Position,long sLength){
            SongName = Name;
            SongLength = SLength;
            SongFile = file;
            SongPosition = Position;
            Length = sLength;
                                                                                             }//Constructor

                    }//SongInfo
                                             }//soundFragment

